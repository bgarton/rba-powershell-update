## RBA PowerShell Updater
### ☁ Complete PowerShell updater for Retailer utilizing HTTPS download. 
- Copy `update.bat` and `update.ps1` into `/data/rba/` dir and run `update.bat`
  - Arguments for `update.bat`:
    - **nodl** - Prevents the download of `update.zip` from the website, e.g. if you already have it in the folder.
    - **noncompare** - Prevents performing an ncompare after updating.
    - **noreindex** - Prevents performing a reindex after updating.