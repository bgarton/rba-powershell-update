# Check our args
if ($args -match "nodl") {
  $download = 0
} else {
  $download = 1
}
if ($args -match "noreindex") {
  $reindex = 0
} else {
  $reindex = 1
}
if ($args -match "noncompare") {
  $ncompare = 0
} else {
  $ncompare = 1
}

$url = "https://rbauk.com/rba_upgrade2/update.zip"
$username = "rba"
$password = "Support1"
$zipName = "update.zip"

# Detect and stop active services
if (Get-Service -Name "SOAPSERVER*") {
  Get-Service -Name "SOAPSERVER*" | Where-Object {$_.Status -eq "Running"} | Stop-Service
}
if (Get-Service -Name "PROCSERVER*") {
  Get-Service -Name "PROCSERVER*" | Where-Object {$_.Status -eq "Running"} | Stop-Service
}

# Basic authentication for downloading zip
$auth = 'Basic ' + [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($username+":"+$password))
$req = New-Object System.Net.WebClient
$req.Headers.Add('Authorization', $auth)

# Make update folder & clear old one
$updateFolderPath = $PSScriptRoot + '\update\'
$oldUpdateFolder = $PSScriptRoot + '\last_update'
$updateFolder = $PSScriptRoot + '\update'
If(!(test-path $updateFolder))
{
  New-Item -ItemType Directory -Force -Path $updateFolderPath
}

# Delete existing files
Remove-Item "$($updateFolderPath)\*" -Recurse

# Download zip file
if ($download -eq 1) {
  Write-Output "Downloading please wait..."
  $req.DownloadFile($url, $updateFolderPath + $zipName)
}

# Create file timestamp of update/folder deletion - optional
#New-Item "$($updateFolderPath)\$(get-date -f yyyyMMdd-hhmmss).txt"

# Extract zip
Write-Output "$($updateFolderPath)"
If(!(test-path "$($updateFolderPath)$($zipName)")) {
  Expand-Archive -Path "$($updateFolderPath)$($zipName)" -Destination $updateFolderPath -Force
}

# Organise structure, zip contains 'major' and 'rbadata' to have rbadata merged onto major
If(!(test-path "$($updateFolderPath)\major")) { 
  New-Item -ItemType Directory -Force -Path "$($updateFolderPath)\major"
}
If(!(test-path "$($updateFolderPath)\rbadata")) { 
  New-Item -ItemType Directory -Force -Path "$($updateFolderPath)\rbadata"
}
Copy-Item -Path "$($updateFolderPath)\major\*" -Destination "$($updateFolderPath)"
Copy-Item -Path "$($updateFolderPath)\rbadata\*" -Destination "$($updateFolderPath)"

# Finally, replace main files
Remove-Item "$($updateFolderPath)\update.zip"
Remove-Item "$($updateFolderPath)\major" -Recurse
Remove-Item "$($updateFolderPath)\rbadata" -Recurse

# Copy update files to RBA folder where the powershell script file is
## Don't forget to rename xppnat as it often gets locked!
$xppnat = "$($PSScriptRoot)\xppnat.dll"
If(test-path "$($xppnat).old") {
  Remove-Item "$($xppnat).old"
}
If(test-path $xppnat) {
  Rename-Item $xppnat "$($xppnat).old"
}

## UPDATE RETAILER
Copy-Item -Path "$($updateFolderPath)\*" -Destination "$PSScriptRoot"

# Update complete! Cleanup by deleting "last_update" and renaming "update" to "last_update"
If(test-path $oldUpdateFolder) {
  Remove-Item $oldUpdateFolder -Recurse
}
Rename-Item $updateFolder $oldUpdateFolder

# Reindex & ncompare
If ($reindex -eq 1) {
  Write-Output "Ncompare..."
  Start-Process -FilePath "rba32" -WorkingDirectory "Live" -ArgumentList "ncompare"
}
If ($ncompare -eq 1) {
  Write-Output "Reindex..."
  Start-Process -FilePath "rba32" -WorkingDirectory "Live" -ArgumentList "reindex"
}

# Detect and start services
Write-Output "Detect and start inactive services..."
if (Get-Service -Name "SOAPSERVER*") {
  Get-Service -Name "SOAPSERVER*" | Where-Object {$_.Status -ne "Running"} | Start-Service
}
if (Get-Service -Name "PROCSERVER*") {
  Get-Service -Name "PROCSERVER*" | Where-Object {$_.Status -ne "Running"} | Start-Service
}

Write-Output "Update complete!"